# My comments on the test

--------------------------------------------------------------------------------

## Architecture

I would normally do comments inline with the code, but in this unusual case of being tested this seems better, since I mostly have broad comments rather than specific and this seems like a fine place to put them.

I'm accustomed to using ui-router and stateful routing, but with the "no-dependencies" rule I started to figure I'd just teach myself the native router, but then I remembered it doesn't even ship with the basic angular install anymore, and I didn't want to risk that counting as a dependency as well.

It seemed as though the assignment was looking for as simple an app as possible, instead. So, while normally I would have routes, and services, and folders for templates and controllers and so forth, I sort of re-taught myself angular for how to do it as this one-template/one-view architecture with just an index and an app.js. I hope I was right in guessing that was what you wanted?

Since I am new to this structure there were a couple places where I felt like there was code smell in my approach. For instance, I would normally have $http run through a separate service, and in trying to stick it in the controller and use it right away I had to stick it in an IIFE. It worked fine, for me so far, but I don't know if that might be an anti-pattern in general. Definitely makes more sense as a service, generally.

## Design

The graphs in the sample picture are prettier than what I got, partly because the data set is less wide. The smallest number in the photo example is 39, I believe? In the data set I was given the numbers range further including 0, 1, etc. This means that if you try to keep your graphs to scale then in order to make your small ones big enough you blow everything up enough that the big ones don't fit anymore.

Since the assignment was to make the graphs in that way, with all the elements on one line, I aimed for a scale somewhere in the middle that would work decently and left it as is. I did a 3 pixel multiplier because that was as big as I could go without the large blocks getting enormous, though it meant some of the small numbers were still so small as to have the same sized block just to fit the number.

If this were a real project, I would speak up and suggest that perhaps having all three (Country, graph 1, graph 2) in a row on one line is not an effective way to display data if we expect such a range of numbers and so many small numbers. We have to factor our expected data range into our design, so my suggestions would then be that:

- While it looks really good having the number show inside the graph bar, if you frequently get 1 and 0 values perhaps we have to settle for putting the number next to the bar instead.
- With such a range, putting the graphs next to each other limits your room for scale. if you had the bars on top of each other, each in their own line, you could blow up the small graph bars much more while still having the full screen width for the larger bars as well.

## Select Box

I have never had trouble with a basic html select box, or one in React or Rails, so it didn't occur to me that this might be a difficulty for me and I didn't allow enough time it seems. My app does work, with the select box created using ng-repeat on the option tag. However, this is less than ideal both because a) the ng-repeat creates lots of watchers and is less efficient than ng-options inside the select tag, and b) this left me with a blank default option instead of one clearly labelled "all".

Still, I left this as my select box solution (with my attempt at the other one commented out) because while this is not ideal it works. Using ng-options, ng-model always returned the value, rather than the key (country name) which broke the rest of my view/hide logic. Even with "track by" keeping my option values equal to the country name, model was still passing "{channels,devices}" type values instead. I'm not sure how you solve this since everything in my research indicated there is no way to pass the key as the model using ng-options.

Off the top of my head, other possible solutions would be reformatting the data a bit before you plug it in to the options so I could get what I needed, or basing my view/hide logic off of a variable on the controller, rather than directly off ng-model, and resetting that variable from the select box based off its value, which I can control, rather than its ng-model, which I apparently cannot control as well as I thought.

## SASS

It says in the assignment that using SASS is encouraged. I didn't choose to, because I felt that it wouldn't save me enough time to be worth the time I'd spend getting it set up. For this small app it felt like using a nuke to swat a fly, and I believe in only using timesaving tools when they actually save you time.

However, since it was encouraged, I feel that I should briefly show that I know how I would have used it. In the case of this app, I would have set the blue and green as variables, since I use those for multiple elements and would want to be able to change them in a quick, consistent way if needed. I would also put a lot of the duplicate css from ".channel" and ".device" in one place and then just use "extend" on each of them and then add the background color as the only different element of each.

## Thank you

I just wanted to express my deep appreciation for you giving me this opportunity. I appreciate the time it will take to go over my code and I thank you for this chance to show you what I can do.

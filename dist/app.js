angular.module('app', [])
  .controller('DataController', DataController);

function DataController($http) {
  this.info = {};
  this.channels = [];
  this.devices = [];
  var self = this;

  (function() {
    $http.get('/data.json')
      .then(function(res) {
        self.info = res.data;
        for (var x in self.info) {
          self.channels.push(self.info[x].Channels);
          self.devices.push(self.info[x].Devices);
        }
        self.channels = self.channels.reduce((total, amount) => total + amount);
        self.devices = self.devices.reduce((total, amount) => total + amount);
      })
  })();

}
